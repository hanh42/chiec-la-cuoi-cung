﻿using project_doan_library.Entity;
using System;
using project_doan_library.Interface;
using System.Collections.Generic;
using System.Text;

namespace project_doan_library
{
    class Book : Base, Ibook
    {
        private static List<Book> _booksList;
        private static int _autoIncreatment = 0;
        private Dictionary<DateTime, Card> _cardsList;
        private int _price;
        private int _releaseYear;
        private int _numberPage;
        private DateTime _inputDate;
        private int _status;
        private readonly Author _author;
        private readonly Publisher _publisher;

        public static List<Book> BooksList { get => _booksList; }
        public int Price { get => _price; set => _price = value; }
        public int ReleaseYear { get => _releaseYear; }
        public int NumberOfPage { get => _numberPage; }
        public DateTime InputDate { get => _inputDate; }
        public int Status { get => _status; set => _status = value; }
        public Author AuthorOfBook => _author;
        public Publisher PublisherOfBook => _publisher;
        public Dictionary<DateTime, Card> CardList { get => _cardsList; }

        /// <summary>
        /// khỏi tạo 1 cuốn sách
        /// </summary>
        /// <param name="name">tên sách được khởi tạo</param>
        /// <param name="price">giá sách được khởi tạo</param>
        /// <param name="releaseYear">năm phát hành </param>
        /// <param name="numPage">số trang </param>
        /// <param name="inputDate">ngày nhập kho</param>
        /// <param name="publisherId">id của nhà phát hành</param>
        /// <param name="authorId">id của tác giả</param>
        public Book(string name = "", int price = 0, int releaseYear = 0, int numPage = 0, string inputDate = "", int publisherId = 0, int authorId = 0):base(name:name)
        {
            if (price < 0)
            {
                throw new Exception("loi khong co gia sach");
            }
            if (numPage <= 0)
            {
                throw new Exception("loi hay nhap so trang");
            }
            if (releaseYear <= 0)
            {
                throw new Exception("loi yeu cau nhap vao nam xuat ban");
            }

            var authorOfBook = Author.ListAuthor.Find(item => item.Id == authorId);
            if (authorOfBook == null)
            {
                throw new Exception("khong ton tai tac gia co id la:" + authorId);
            }

            var publisherOfBook = Publisher.PublisherList.Find(item => item.Id == publisherId);
            if (publisherOfBook == null)
            {
                throw new Exception("khong tim thay nha san xuat co id:" + publisherId);
            }
            
            try
            {
                this._inputDate = Convert.ToDateTime(inputDate);
            }
            catch (Exception)
            {
                throw new Exception("Dinh dang ngay nhap loi");
            }
            
            if (_booksList == null)
            {
                _booksList = new List<Book>();
            }
            
            base._id = ++_autoIncreatment;
            base.Name = name;
            this._status = 0;
            this._price = price;
            this._releaseYear = releaseYear;
            this._numberPage = numPage;
            this._cardsList = new Dictionary<DateTime, Card>();
            this._author = authorOfBook;
            this._publisher = publisherOfBook;
            this._publisher.PublisherBooksList.Add(this);
            this._author.BooksList.Add(this);
            _booksList.Add(this);
        }

        /// <summary>
        /// lấy dữ liệu để ghi file
        /// </summary>
        /// <returns></returns>
        public static string[] ToFile()
        {
            List<string> dataOfAuthor = new List<string>();
            dataOfAuthor.Add($"{_autoIncreatment}");
            if (_booksList != null)
            {
                for (int i = 0; i < _booksList.Count; i++)
                {
                    dataOfAuthor.Add($"{_booksList[i].Id}-{_booksList[i].Name}" +
                   $"-{_booksList[i].Price}-{_booksList[i].ReleaseYear}" +
                   $"-{_booksList[i].NumberOfPage}-{_booksList[i].InputDate}-{_booksList[i]._publisher.Id}" +
                   $"-{_booksList[i]._author.Id}");
                }
            }
            //add o day la add cac phan tu cua list a
            return dataOfAuthor.ToArray();
        }
    }
}
