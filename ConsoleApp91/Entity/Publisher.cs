﻿using System;
using System.Collections.Generic;
using System.Text;
using project_doan_library.Interface;

namespace project_doan_library.Entity
{
    class Publisher : Base, IPublisher
    {
        private static List<Publisher> _publishersList;
        private List<Book> _booksList;
        private static int _autoIncreatment = 0;

        public static List<Publisher> PublisherList { get => _publishersList; }
        public List<Book> PublisherBooksList { get => _booksList; }

        /// <summary>
        /// khởi tạo 1 nhà xuất bản
        /// </summary>
        /// <param name="name">tên nhà xuất bản</param>
        public Publisher(string name = "") : base(name: name)
        {
            if (_publishersList == null)
            {
                _publishersList = new List<Publisher>();
            }
            base._id = ++_autoIncreatment;
            this.Name = name;
            _booksList = new List<Book>();
            _publishersList.Add(this);
        }

        /// <summary>
        /// lấy dữ liệu cần ghi vào file
        /// </summary>
        /// <returns>chuỗi dữ liệu để ghi vào file</returns>
        public static string[] ToFile()
        {
            List<string> dataOfAuthor = new List<string>();
            dataOfAuthor.Add($"{_autoIncreatment}");
            if (_publishersList != null)
            {
                for (int i = 0; i < _publishersList.Count; i++)
                {
                    dataOfAuthor.Add($"{_publishersList[i].Id}-{_publishersList[i].Name}");
                }
            }
            //add o day la add cac phan tu cua list a
            return dataOfAuthor.ToArray();
        }
    }
}

