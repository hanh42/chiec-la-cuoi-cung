﻿using System;
using System.Collections.Generic;
using System.Text;
using project_doan_library.Interface;

namespace project_doan_library.Entity
{
    class User:Base,IUser
    {
        private string _password;

        public string Password { get => _password; set => _password = value; }

        /// <summary>
        ///khởi tạo 1 user
        /// </summary>
        /// <param name="name">tên user</param>
        /// <param name="password">mật khẩu user</param>
        public User(string name="", string password=""):base(name:name)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("loi nhap mat khau!");
            }

            this._password = password;
        }
    }
}
