﻿using System;
using System.Collections.Generic;
using System.Text;

namespace project_doan_library.Entity
{
    class BookCard
    {
        private static List<BookCard> _bookCard;
        private int _bookId;
        private int _cardId;

        public static List<BookCard> Reader_sloanSlip { get => _bookCard; set => _bookCard = value; }

        /// <summary>
        /// khởi tạo lớp quan hệ giữa sách và phiếu mượn [N-N] => 1-N || N-1 
        /// </summary>
        /// <param name="bookId">mã sách</param>
        /// <param name="cardId">mã phiếu mượn</param>
        public BookCard(int bookId = 0, int cardId = 0)
        {
            var book = Book.BooksList.Find(item => item.Id == bookId);
            var loanSlip = Card.CardList.Find(item => item.Id == cardId);

            if (book == null)
            {
                throw new Exception("khong co sach nao co id:" + bookId);
            }

            if (loanSlip == null)
            {
                throw new Exception("khong ton tai phieu muon co id:" + cardId);
            }
          
            this._bookId = bookId;
            this._cardId = cardId;
            _bookCard.Add(this);
        }
    }
}
