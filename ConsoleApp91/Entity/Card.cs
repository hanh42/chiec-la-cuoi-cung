﻿using System;
using System.Collections.Generic;
using System.Text;
using project_doan_library.Interface;

namespace project_doan_library.Entity
{
    class Card :Base,ICard
    {
        private static List<Card> _cardslist;
        private static int _autoIncreatment = 0;
        private Book _bookCard;
        private Reader _readerCard;
        private DateTime _bookLoanDate;
        private DateTime _bookReturnDate;
        private int _status;
        private int _numberCard;

     
        public DateTime BookLoanDate { get => _bookLoanDate; }
        public DateTime BookReturnDate { get => _bookReturnDate; }
        public int Status { get => _status; set => _status = value; }
        public int NumberCard { get => _numberCard; }
        public static List<Card> CardList { get => _cardslist; }
        public Book BookCard { get => _bookCard; set => _bookCard = value; }
        public Reader ReaderCard { get => _readerCard; set => _readerCard = value; }

        /// <summary>
        /// khởi tạo phiếu mượn
        /// </summary>
        /// <param name="bookId">id của sách</param>
        /// <param name="ReaderId">id của người mượn của phiếu</param>
        public Card(int bookId = 0, int ReaderId = 0)
        {
            var readerCard = Reader.ReaderList.Find(item => item.Id == ReaderId);
            if (readerCard == null)
            {
                throw new Exception("khong ton tai khach hang co id:" + _id);
            }

            var book = Book.BooksList.Find(item => item.Id == bookId);
            if (book == null)
            {
                throw new Exception("khong ton tai sach co id:" + bookId);
            }
            else
            {
                if (book.Status != 0)
                {
                    throw new Exception("khong the tao phieu muon cho cuon sach co id:[" + bookId + "] da duoc muon boi:" + Card.CardList.Find(item => item._bookCard.Id == bookId)._readerCard);
                }
                else
                {
                    book.Status = 1;
                }
            }

            if (CardList == null)
            {
                _cardslist = new List<Card>();
            }
           
            base._id = ++_autoIncreatment;
            this._bookCard = book;
            DateTime datenow = DateTime.Now;
            //ngay muonlay ngay hien tai!
            this._bookLoanDate = DateTime.Now;
            this._bookReturnDate = this._bookLoanDate.AddDays(7);
            this._status = 1;
            this._readerCard = readerCard;
            this._readerCard.ReaderCardList.Add(this);
            this._numberCard = readerCard.ReaderCardList.Count;
            this._bookCard.CardList.Add(this._bookLoanDate, this);
            CardList.Add(this);
        }

        /// <summary>
        /// lấy dữ liệu cần ghi vào file
        /// </summary>
        /// <returns>chuỗi dữ liệu để ghi vào file</returns>
        public static string[] ToFile()
        {
            List<string> dataOfAuthor = new List<string>();
            dataOfAuthor.Add($"{_autoIncreatment}");
            if (_cardslist != null)
            {
                for (int i = 0; i < _cardslist.Count; i++)
                {
                    dataOfAuthor.Add($"{_cardslist[i].Id}" +
                        $"-{_cardslist[i]._bookCard.Id}" +
                        $"-{_cardslist[i]._readerCard.Id}");
                }
            }
            //add o day la add cac phan tu cua list a 
            return dataOfAuthor.ToArray();
        }
    }
}