﻿using System;
using System.Collections.Generic;
using System.Text;
using project_doan_library.Interface;
namespace project_doan_library.Entity
{
    class Author : Base, IAuthor
    {
        private static List<Author> _authorsList;
        private List<Book> _booksList;
        private static int _autoIncreatment = 0;

        public static List<Author> ListAuthor { get => _authorsList; }
        public List<Book> BooksList { get => _booksList; }

        /// <summary>
        /// dùng để khỏi tạo 1 tác giả
        /// </summary>
        /// <param name="name">tên tác giả</param>
        public Author(string name = "") : base(name: name)
        {
            if (_authorsList == null)
            {
                _authorsList = new List<Author>();
            }
            base.Name = name;
            base._id = ++_autoIncreatment;
            this._booksList = new List<Book>();
            _authorsList.Add(this);
        }
    }
}
