﻿using System;
using System.Collections.Generic;

namespace project_doan_library.Services
{
    public interface IKeyValue
    {
        public string GetKey();
        public string GetValueType();
        public object GetValue();
    }
    public class KeyValue<T> : IKeyValue
    {
        private T _value;
        private string _key;

        public string Key { get => _key; set => _key = value; }
        public T Value { get => _value; set => _value = value; }
        public bool IsPassword { get; set; }

        public KeyValue(string key, T value)
        {
            Key = key;
            Value = value;
        }

        public KeyValue(string key)
        {
            Key = key;
        }

        public KeyValue(string key, T value, bool isPassword)
        {
            Key = key;
            Value = value;
            IsPassword = isPassword;
        }

        public string GetKey()
        {
            return Key;
        }

        public string GetValueType()
        {
            return _value.GetType().Name;
        }

        public object GetValue()
        {
            return Value;
        }
    }
    public static class View
    {
        private static int length = 25;

        public static int Length { set => length = value; }

        /// <summary>
        /// chức năng nhập mật khẩu
        /// </summary>
        /// <returns></returns>
        public static string InputPassword()
        {
            var pass = string.Empty;
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && pass.Length > 0)
                {
                    Console.Write("\b \b");
                    pass += key;
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    pass += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Black;
            return pass;
        }

        /// <summary>
        /// tự động tạo chuỗi theo số lượng
        /// </summary>
        /// <param name="n">số lượng</param>
        /// <param name="str">chuỗi</param>
        /// <returns>chuỗi đã tạo</returns>
        public static string GenStr(int n, string str)
        {
            string strs = "";
            for (int i = 0; i < n; i++)
            {
                strs += str;
            }
            return strs;
        }

        /// <summary>
        /// Hiển thị tiêu đề
        /// </summary>
        /// <param name="title">tên tiêu đề</param>
        public static void HeaderTitle(string title)
        {
            title = title.ToUpper();
            int titleLength = title.Length;
            int lenght = ((length) * 2 - 2 - 1 - titleLength) / 2;
            bool checkLength = (length * 2 - 1) == (titleLength + (lenght * 2) + 2);
            Console.Clear();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\t");
            for (int i = 0; i < length; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
            Console.Write("\t");
            Console.Write("*" + GenStr(lenght, " "));
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(title);
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (checkLength)
            {
                Console.WriteLine(GenStr(lenght, " ") + "*");
            }
            else
            {
                Console.WriteLine(GenStr(lenght + 1, " ") + "*");
            }

            Console.Write("\t");
            for (int i = 0; i < length; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
            //awdawd
            Console.ForegroundColor = ConsoleColor.Black;
        }

        /// <summary>
        /// nhập dữ liệu
        /// </summary>
        /// <param name="data">dữ liệu cần nhập
        ///     <param name="Key">
        ///         câu xuất ra màn hình
        ///     </param>
        ///     <param name="Value">
        ///         dữ liệu cần nhập
        ///     </param>
        /// </param>
        public static void Input(List<IKeyValue> data)
        {
            int maxLengthKey = GetMaxLength(data);

            int lenght = length - maxLengthKey - 2;
            int itemLenght = 0;

            bool checkInput = false;

            foreach (var dataItem in data)
            {
                do
                {
                    itemLenght = dataItem.GetKey().Length;
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.Write($"\t{GenStr(lenght, " ")}{dataItem.GetKey()}");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write($"{GenStr(maxLengthKey - itemLenght, " ")} : ");



                    switch (dataItem.GetValueType())
                    {
                        case "Int32":
                            int intValue;
                            checkInput = !int.TryParse(Console.ReadLine(), out intValue);
                            ((KeyValue<int>)dataItem).Value = intValue;
                            break;

                        case "Double":
                            double doubleValue;
                            checkInput = !double.TryParse(Console.ReadLine(), out doubleValue);
                            ((KeyValue<double>)dataItem).Value = doubleValue;
                            break;

                        case "String":
                            if (((KeyValue<string>)dataItem).IsPassword)
                            {
                                ((KeyValue<string>)dataItem).Value = InputPassword();
                            }
                            else
                            {
                                ((KeyValue<string>)dataItem).Value = Console.ReadLine();
                            }
                            break;

                        case "Char":
                            ((KeyValue<char>)dataItem).Value = Console.ReadKey().KeyChar;
                            Console.WriteLine();
                            break;
                    }
                } while (checkInput);
            }
            Console.ForegroundColor = ConsoleColor.Black;
        }

        /// <summary>
        /// hiển thị menu
        /// </summary>
        /// <param name="data">dữ liệu cần nhập
        ///     <param name="Key">
        ///         lựa chọn
        ///     </param>
        ///     <param name="Value">
        ///         thực hiện
        ///     </param>
        /// </param>
        public static void SelectItemMenu(List<IKeyValue> data)
        {
            int maxLengthKey = GetMaxLength(data);

            int lenght = length - maxLengthKey - 2;
            int itemLenght = 0;

            foreach (var dataItem in data)
            {
                itemLenght = dataItem.GetKey().Length;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write($"\t{GenStr(lenght, " ")}{dataItem.GetKey()}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"{GenStr(maxLengthKey - itemLenght, " ")} : {((KeyValue<string>)dataItem).Value}");
            }
            Console.ForegroundColor = ConsoleColor.Black;
        }

        /// <summary>
        /// lây độ dài tối đa của key
        /// </summary>
        /// <param name="data">mảng KeyValue</param>
        /// <returns></returns>
        public static int GetMaxLength(List<IKeyValue> data)
        {
            int maxLength = int.MinValue;

            foreach (var item in data)
            {
                int itemLenght = item.GetKey().Length;
                if (itemLenght > maxLength)
                {
                    maxLength = itemLenght;
                }
            }

            Console.ForegroundColor = ConsoleColor.Black;
            return maxLength;
        }

        /// <summary>
        /// cảnh báo
        /// </summary>
        /// <param name="str">chuỗi hiển thị</param>
        public static void Warning(string str)
        {
            int lenght = length - (str.Length / 2) - 1;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\t{GenStr(lenght, " ")}{str}");
            Console.ReadKey();
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
