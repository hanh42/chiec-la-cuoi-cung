﻿using System;
using System.Collections.Generic;
using System.Text;

namespace project_doan_library.Services
{
    static class Function
    {

        Console.WriteLine("ggghhhhhhhhhhhhh");
        /// <summary>
        /// lấy dữ liệu user từ screen và so sánh  với dữ liệu trong file
        /// </summary>
        /// <param name="username">tên acc do người dùng nhập vào</param>
        /// <param name="passworld">mật khẩu do người dùng nhập vào </param>
        /// <returns>kết quả việc đăng nhập</returns>
        internal static bool Login(string username = "", string passworld = "")
        {
            return true;
        }

        /// <summary>
        /// lấy kết quả lựa chọn Quản lý sách vs Quản lý phiếu mượn.
        /// </summary>
        /// <param name="choose">chức năng do người dùng lựa chọn</param>
        internal static void Mainfunction(int choose)
        {
            switch (choose)
            {
                case 1:
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// lựa chọn các chức năng con của chức năng cha mà bạn đã chọn
        /// </summary>
        /// <param name="choose">lựa chọn chức năng con</param>
        internal static void Childfunction(int choose)
        {
            switch (choose)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// hiển thị thông tin tất cả các cuốn sách
        /// </summary>
        /// <param name="id">id của sách mà bạn muốn xuất thông tin</param>
        internal static void BooksIformation()
        {
        }

        /// <summary>
        /// thêm sách vào thư viện
        /// </summary>
        /// <param name="name">tên sách</param>
        /// <param name="price">giá</param>
        /// <param name="releaseYear">năm phát hành</param>
        /// <param name="numPage">số trang</param>
        /// <param name="inputDate">ngày nhập kho</param>
        /// <param name="publisherId">mã nhà xuất bản</param>
        /// <param name="authorId">mã tác giả</param>
        /// <returns>kết quả việc tạo sách</returns>
        internal static bool BookAdd(string name = "", int price = 0, int releaseYear = 0, int numPage = 0, string inputDate = "", int publisherId = 0, int authorId = 0)
        {
            return true;
        }

        /// <summary>
        /// xóa sách
        /// </summary>
        /// <param name="BookId">mã sách cần xóa</param>
        /// <returns>kết quả việc xóa sách</returns>
        internal static bool BookDelete(int BookId = 0)
        {
            return true;
        }

        /// <summary>
        /// hiển thị thông tin của tất cả các phiếu mượn
        /// </summary>
        internal static void CardsInformation()
        {
        }

        /// <summary>
        ///Mượn sách
        /// </summary>
        /// <param name="bookId">mã sách</param>
        /// <param name="ReaderId">mã bạn đọc</param>
        /// <returns></returns>
        internal static bool CardAdd(int bookId = 0, int ReaderId = 0)
        {
            return true;
        }

        /// <summary>
        /// trả sách
        /// </summary>
        /// <param name="cardId">mã sách mà bạn muốn trả</param>
        /// <returns></returns>
        internal static bool BookPay(int cardId = 0)
        {
            return true;
        }
    }
}
